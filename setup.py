#!/usr/bin/env python

"""
setup.py file for SWIG-ified wndchrm
"""

from setuptools import setup, Extension

# Need to subclass setuptools/distutils objects to change build order
# such that dynamically-generated swig files will be present when MANIFEST is created.
# See http://stackoverflow.com/a/21236111/1017549

import os, sys

__version__ = None
def GetVersions ():
	print ('Getting version info')
	pkg_base = os.path.dirname( os.path.realpath(__file__))
	pkg_dir = os.path.join(pkg_base, 'CharmFeatures' )
	PY3 = sys.version_info.major == 3
	PY2 = sys.version_info.major == 2
	# this sets the __version__ variable initially to the library version
	# In the library's code distribution the version string is in the file src/VERSION
	print ('working from: ',pkg_base)
	with open(os.path.join (pkg_base, 'src', 'VERSION')) as f:
		__lib_version__ = f.read().strip()
		with open( os.path.join( pkg_dir, '_lib_version.py' ), 'w+' ) as f:
			f.write( "__lib_version__ = '{0}'\n".format( __lib_version__) )
		print ('wrote {} to {}: '.format(__lib_version__, os.path.join( pkg_dir, '_lib_version.py' )))

	__version__ = __lib_version__
	if PY3:
		exec(open(os.path.join (pkg_dir,'_version.py')).read())
	else:
		execfile(os.path.join (pkg_dir,'_version.py'))

	try:
		from subprocess import check_output
		with open(os.devnull, 'w') as DEVNULL:
			os.chdir (pkg_base)
			git_hash = check_output(['git', 'rev-parse', '--short', 'HEAD'], stderr=DEVNULL ).decode("utf-8").strip()
			# Check for local modifications
			localmod = check_output(['git', 'diff-index', '--name-only', 'HEAD'], stderr=DEVNULL ).decode("utf-8").strip()
			if len(localmod) > 0:
				# print ('localmod\n', localmod, "\n")
				git_hash += '.localmod'
			print ("Building WND-CHARM {} at git repo commit {}...".format( __version__, git_hash ))
		# this construction matches what is done in __init__.py by importing
		# both _version.py and _git_hash.py use "normalized" semantic version string (a.k.a., dots)
		__version__ = __version__+ '+' + git_hash
		with open( os.path.join( pkg_dir, '_git_hash.py' ), 'w+' ) as f:
			f.write( "__git_hash__ = '{0}'\n".format( git_hash) )
	except Exception as e:
		print ("Building WND-CHARM {} release version...".format( __version__ ))
	return (__version__)


__version__ = GetVersions()


libcharm_module = Extension('libcharm',
	sources=[
		'src/colors/FuzzyCalc.cpp',
		'src/statistics/CombFirst4Moments.cpp',
		'src/statistics/FeatureStatistics.cpp',
		'src/textures/gabor.cpp',
		'src/textures/haralick/CVIPtexture.cpp',
		'src/textures/haralick/haralick.cpp',
		'src/textures/tamura.cpp',
		'src/textures/zernike/complex.cpp',
		'src/textures/zernike/zernike.cpp',
		'src/transforms/ChebyshevFourier.cpp',
		'src/transforms/chebyshev.cpp',
		'src/transforms/radon.cpp',
		'src/transforms/wavelet/Common.cpp',
		'src/transforms/wavelet/convolution.cpp',
		'src/transforms/wavelet/DataGrid2D.cpp',
		'src/transforms/wavelet/DataGrid3D.cpp',
		'src/transforms/wavelet/Filter.cpp',
		'src/transforms/wavelet/FilterSet.cpp',
		'src/transforms/wavelet/Symlet5.cpp',
		'src/transforms/wavelet/Wavelet.cpp',
		'src/transforms/wavelet/WaveletHigh.cpp',
		'src/transforms/wavelet/WaveletLow.cpp',
		'src/transforms/wavelet/WaveletMedium.cpp',
		'src/transforms/wavelet/wt.cpp',
		'src/cmatrix.cpp',
		'src/libcharm.cpp',
		'src/ImageTransforms.cpp',
		'src/FeatureAlgorithms.cpp',
		'src/Tasks.cpp',
		'src/FeatureNames.cpp',
		'src/gsl/specfunc.cpp',
		'src/capi.cpp',
	],
	include_dirs=['./','src/', '/usr/local/include'],
	libraries=['tiff','fftw3'],
)

local_lib_dir = '/home/igg/Projects/charm-features/src/.libs/'
GetVersions

# sudo apt-get install gcc libpq-dev -y
# sudo apt-get install python-dev  python-pip -y
# sudo apt-get install python3-dev python3-pip python3-venv python3-wheel -y
# pip3 install wheel
setup (
	name = 'CharmFeatures',
	version = __version__,
	author      = "Ilya Goldberg, Nikita Orlov, Josiah Johnston, Lior Shamir, Chris Coletta",
	author_email = "igg <at> iggtec <dot> com",
	url = 'https://gitlab.com/iggman/charm-features',
	description = """Python bindings for charm features from wnd-charm""",
	license = 'LGPLv2',
	# N.b.: the module name has to match in CharmFeatures/__init__.py
	ext_modules = [libcharm_module],
	packages = ['CharmFeatures'],
	setup_requires=['wheel'],
	# bitarray is for pylibtiff
	install_requires=['numpy<2', 'scipy', 'tables', 'pylibtiff', 'fasteners', 'bitarray'],
    entry_points = {
        'console_scripts': ['charm-image-features=CharmFeatures.ProcessImages:main'],
    }
)
