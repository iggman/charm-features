FROM python:3.10 AS charmfeatures
RUN apt-get update -qq && apt-get install  -y --no-install-recommends  \
	build-essential libfftw3-dev libtiff-dev libhdf5-dev
COPY . /home/src/charm-features
# make sure we get clean build with a recompile in case there's stuff in the local build directory
RUN rm -rf /home/src/charm-features/build /home/src/charm-features/CharmFeatures.egg-info
RUN umask 000; pip install --upgrade pip
RUN umask 000; pip install --no-cache-dir 'numpy<2' scipy tables pylibtiff fasteners bitarray
RUN umask 000; pip install --no-cache-dir --no-binary :all: /home/src/charm-features
# charm-image-features -t4 -n -o test-t4.npz ../images
# ENTRYPOINT ["charm-image-features -h"]
ENTRYPOINT []